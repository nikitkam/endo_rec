#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess as sp
import time


def run_ffmpeg():
    FFMPEG_BIN = 'ffmpeg'

    # ffmpeg -f x11grab -r 25 -s 1366x768 -i :0.0 -vcodec libx264 video.mkv
    # ffmpeg -y -f x11grab -s 1366x768 -qscale 10 -i :0.0  -vcodec libx264  video.mkv -r 1 -s 1366x768 -f image2 foo-%03d.png


    command = [ FFMPEG_BIN,
                '-y',
                '-f', 'x11grab',
                '-r', '25',
                '-s', '1366x768',
                '-i', ':0.0',
                '-vcodec', 'libx264',
                'video.mkv',
                '-r', '1',
                '-s', '1366x768',
                '-f', 'image2',
                'foo-%05d.png'
    ]

    # pipe = sp.Popen(command, stdout = sp.PIPE, stdin = sp.PIPE, bufsize=10**8)
    pipe = sp.Popen(command, stdin = sp.PIPE, bufsize=10**8)

    # pipe = sp.Popen(command, stdout = sp.PIPE, bufsize=10**8)

    print 'Popen\nWaiting...'

    # print pipe.stdout.read()
    # time.sleep(10)
    # pipe.terminate()
    # q = 'q'
    # pipe.communicate(q)

    # print 'sent q'
    return pipe





if __name__ == '__main__':
    run_ffmpeg()
