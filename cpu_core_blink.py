#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess as sp
import time

def changeRedState(temp):
    p = sp.Popen(['./turn_on_diode', '0', 'o'], stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = p.communicate()
    readReg = int(out[out.find('reg=') + len('reg=')])
    if temp > 90:
        p = sp.Popen(['./turn_on_diode', str(readReg | 4), 'i'], stdout=sp.PIPE, stderr=sp.PIPE)
        out, err = p.communicate()
    else:
        p = sp.Popen(['./turn_on_diode', str(readReg ^ 4), 'i'], stdout=sp.PIPE, stderr=sp.PIPE)
        out, err = p.communicate()



def turn_on_led(led_num):
    command = [ './turn_on_diode', str(led_num), 'i']
    pipe = sp.Popen(command, stdin=sp.PIPE)


if __name__ == '__main__':
    prevTemp = 0
    thresVal = 80
    while True:
        p = sp.Popen(['sensors'], stdout=sp.PIPE, stderr=sp.PIPE)
        out, err = p.communicate()
        tempStart = out.find("Physical id 0")
        curTemp = int(out[tempStart+17:tempStart+19])
        print(curTemp)
        if curTemp > thresVal: #80
            changeRedState(curTemp)
        if curTemp <= thresVal and prevTemp > thresVal:
            p = sp.Popen(['./turn_on_diode', '0', 'o'], stdout=sp.PIPE, stderr=sp.PIPE)
            out, err = p.communicate()
            readReg = int(out[out.find('reg=') + len('reg=')])
            p = sp.Popen(['./turn_on_diode', str(readReg & 3), 'i'], stdout=sp.PIPE, stderr=sp.PIPE)
            out, err = p.communicate()
        time.sleep(0.25)
        prevTemp = curTemp
