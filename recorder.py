#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
from cv2.cv import *

import datetime as dt
import glob
import numpy as np
import os
import signal
import shutil
import subprocess as sp
import sys
import time

from find_path import get_records_dir

# ========================== настройка логирования =============================
import logging
from logging.handlers import RotatingFileHandler

logging.basicConfig(level=logging.INFO, fmt='')

# формат сообщений в логах
FORMATTER = logging.Formatter(
      fmt='%(asctime)s [%(name)s][%(levelname)s]: %(message)s'
    , datefmt='%d/%m/%Y %H:%M:%S'
)


_SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))

# хэндлер, который в лог пишет все
_LOG_FILENAME = os.path.join(_SCRIPT_DIR, 'spam.log')
spam_h = RotatingFileHandler(
      filename=_LOG_FILENAME
    , maxBytes=50*1024*1024
    , backupCount=5
    , encoding='utf-8'
)
spam_h.setLevel(logging.DEBUG)
spam_h.setFormatter(FORMATTER)

logger = logging.getLogger(__name__)
logger.addHandler(spam_h)
# ======================== конец настройки логирования ========================




def remove_tmp_photos():
    # удаляем временные фотки экрана
    logger.debug("removing temp photos...")
    tmp_photos_names = os.path.join(_SCRIPT_DIR, 'tmp*.png')
    for f in glob.glob(tmp_photos_names):
        os.remove(f)


def run_ffmpeg(resolution, of=None):
    # Перед тем, как записывать, удаляем временные фотографии.
    remove_tmp_photos()

    FFMPEG_BIN = 'ffmpeg'
    if not of:
        of = 'video.mkv'

# ffmpeg -y -f v4l2 -vsync 2 -pix_fmt yuv420p -s 1920x1080 -i /dev/video1 -vcodec libx264 video.mp4 -r 1 -s 1920x1080 -f image2 foo-%03d.png
# -vcodec h263p -qscale 4
    command = [ FFMPEG_BIN,
                '-y',
                '-f', 'v4l2',
                '-vsync', '2',
                '-pix_fmt', 'yuv420p',
                '-s', resolution,
                # TODO: определение существующего устройства /dev/videoX
                '-i', '/dev/video0',
                '-vcodec', 'h263p',
                '-qscale', '4',
                of,
                '-r', '1',
                '-s', resolution,
                '-f', 'image2',
                'tmp%04d.png'
    ]

    pipe = sp.Popen(command, stdin=sp.PIPE, bufsize=10**8)

    logger.debug('Popen\nWaiting...')
    return pipe


def stop_ffmpeg(pipe_):
    FFMPEG_STOP_KEY = 'q'
    if pipe_:
        pipe_.communicate(FFMPEG_STOP_KEY)
    else:
        logger.warning('Empty pipe!! Something has gone wrong!')



def checkIfStopwachOnFrame(frameroi, trackEdges):
    """define if there is a stopwatch on a screen"""
    if frameroi is None:
        return False
    # get edges on roi frame
    avrgVal = np.average(np.asarray(frameroi))
    #avrgVal-5, avrgVal+5)#min(200,100 + avrgVal))
    frameEdges = cv2.Canny(np.asarray(frameroi), 127, 127)
    result = cv2.bitwise_and(frameEdges, trackEdges)
    covered = ( 100 * sum(sum(result)) ) / ( sum(sum(trackEdges)))
    if avrgVal > 200:
        # image too bright to detect symbols
        return None
    if covered < 60:
        return False
    else:
        return True

def getROIbounds(frame):
    ret, frame = cv2.threshold(frame, 127, 255, cv2.THRESH_BINARY)
    frame = cv2.cvtColor(frame, cv2.cv.CV_RGB2GRAY)
    trackMaxH = max((np.where(frame == np.max(frame)))[0])+1
    trackMinH = min((np.where(frame == np.max(frame)))[0])-1
    trackMaxW = max((np.where(frame == np.max(frame)))[1])+1
    trackMinW = min((np.where(frame == np.max(frame)))[1])-1
    return [trackMaxH, trackMinH, trackMaxW, trackMinW]


def defineResolution():
    cmnd = ['ffprobe', '-f', 'v4l2', '-v', 'quiet', '-show_streams', '/dev/video0']
    #cmnd = ['ffprobe', '-v', 'quiet', '-show_streams','videoFHD.mp4']
    p = sp.Popen(cmnd, stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = p.communicate()
    if not out:
        return None

    widthPosStart = out.find('width=')
    if widthPosStart == -1:
        return None
    widthPosStart += len('width=')
    widthPosEnd = out.find('\n', widthPosStart)

    heightPosStart = out.find('height=')
    if heightPosStart == -1:
        return None
    heightPosStart += len('height=')
    heightPosEnd = out.find('\n', heightPosStart)

    res = out[widthPosStart:widthPosEnd] + 'x' + out[heightPosStart:heightPosEnd]
    logger.info('resolution is %s', res)
    return res

def getMaskPercentCovered(frame, mask):
    if frame is None:
        logger.critical('[-] empty frame!')
        sys.exit(1)
    result = cv2.bitwise_and(frame, mask)
    ret, result = cv2.threshold(result,127,255,cv2.THRESH_BINARY)
    result = cv2.cvtColor(result, cv2.cv.CV_RGB2GRAY)
    ret, mask = cv2.threshold(mask,127,255,cv2.THRESH_BINARY)
    mask = cv2.cvtColor(mask, cv2.cv.CV_RGB2GRAY)
    ret = ( 100 * sum(sum(result))/( np.amax(result) + 1 ) ) / ( sum(sum(mask))/( np.amax(mask) + 1 ))
    #print(ret)
    return ret


def getLastPicName():
    """
        Функция возвращает путь последнего tmp снимка
    """
    tmp_photos_names = os.path.join(_SCRIPT_DIR, 'tmp*.png')
    tmp_photos = sorted(glob.glob(tmp_photos_names))
    if not len(tmp_photos):
        logger.debug("Can't find files %s", tmp_photos_names)
        return None
    return tmp_photos[-1]


class LED_LIGHTS:
    YELLOW = 1
    GREEN = 2
    # пин 3 почему-то загорается самостоятельно. Пока не выясню почему - красный диод перемещаем на пин 4
    RED = 8
    HDD = 16
    ALL = 11
    NONE = 0

def turn_on_led(led_num):
    command = [ './turn_on_diode', str(led_num), 'i']
    pipe = sp.Popen(command, stdin=sp.PIPE)


_MASK_DIR = os.path.join(_SCRIPT_DIR, 'masks')
def getTrack(frame, resolution):
    if not os.path.exists(_MASK_DIR):
        logger.critical("There is no masks dir %s!", _MASK_DIR)
        sys.exit(-1)

    mask_glob = os.path.abspath(_MASK_DIR) + '/' + resolution + '*' + 'mask.bmp'
    track_glob = os.path.abspath(_MASK_DIR) + '/' + resolution + '*' + 'track.bmp'
    masks = sorted(glob.glob(mask_glob))
    tracks = sorted(glob.glob(track_glob))
    masks_tracks = dict(zip(masks, tracks))

    if not masks or not tracks:
        logger.critical("There is no masks found: masks: %s, tracks: %s!", masks, tracks)
        sys.exit(-1)

    max_coverage = 0
    for mask_filename in masks:
        mask = cv2.imread(mask_filename)
        coverage = getMaskPercentCovered(frame, mask)
        if coverage > max_coverage:
            max_coverage = coverage
            return_trackname = masks_tracks[mask_filename]

    if max_coverage < 50:
        return None

    track = cv2.imread(return_trackname)
    return track


def copy_new_masks(records_dir):
    flash_masks_dir = os.path.join(records_dir, '../vidicap_configuration')
    flash_masks_dir = os.path.normpath(flash_masks_dir)
    if not os.path.exists(flash_masks_dir):
        return

    global _MASK_DIR
    shutil.rmtree(_MASK_DIR)
    shutil.copytree(flash_masks_dir, _MASK_DIR)
    logger.info("New masks from %s successfully copied into %s", flash_masks_dir, _MASK_DIR)



def main():
    turn_on_led(LED_LIGHTS.ALL)
    # ждем пока загрузится система и подмонтируется жесткий диск
    time.sleep(10)
    #моргание диодом от температуры пока что не запускаем
    #pipe = sp.Popen(['python', 'cpu_core_blink.py'], stdin=sp.PIPE)

    # Определнеие flash накопителя, проверка новых масок на нем, копирование лога.
    spaceNotFound = True
    while spaceNotFound:
        records_dir = get_records_dir()
        if not records_dir:
            turn_on_led(LED_LIGHTS.HDD)
            time.sleep(2)
            turn_on_led(LED_LIGHTS.NONE)
            time.sleep(2)
            logger.warning('No media device found! Waiting...')
        else:
            copy_new_masks(records_dir)
            copy_log_onto_flash(records_dir)
            spaceNotFound = False


    resolution = defineResolution()
    while not resolution:
        logger.debug("Can't define resolution! Waiting for 5 sec.")
        turn_on_led(LED_LIGHTS.RED)
        time.sleep(0.5)
        turn_on_led(LED_LIGHTS.NONE)
        time.sleep(0.5)
        turn_on_led(LED_LIGHTS.RED)
        time.sleep(0.5)
        turn_on_led(LED_LIGHTS.NONE)
        time.sleep(0.5)
        resolution = defineResolution()

    recording = False
    cap = cv2.VideoCapture(-1)
    if not cap:
        turn_on_led(LED_LIGHTS.RED)
        logger.critical('VideoCapture(-1) error! -> Exit')
        sys.exit(1)

    #determine what position of stopwatch to track
    #1 skip first several frames
    for i in range(1, 10):
        ret, frame = cap.read()

    # проверка на четкое равенство None (is None), т.к. в случае ошибки
    # frame устанавливается в None. Не используем == из-за предупреждения:
    # FutureWarning: comparison to `None` will result in an elementwise
    # object comparison in the future.
    if frame is None:
        logger.critical("cap.read() failed! frame is empty -> Exit!")
        sys.exit(1)


    track = getTrack(frame, resolution)

    if track == None:
        logger.critical('There is no mask image that adopts frame captured!')
        sys.exit(1)
    maxH, minH, maxW, minW = getROIbounds(track)
    trackroi = cv2.cv.fromarray(track[minH:maxH, minW:maxW].copy())
    avrgVal = np.average(np.asarray(trackroi))
    trackEdges = cv2.Canny(np.asarray(trackroi), avrgVal, min(200,100 + avrgVal))


    logger.debug('Entering main loop.')
    turn_on_led(LED_LIGHTS.YELLOW)
    pipe = None

    # variabl to check if ffmpeg still works
    ffmpeg_stacked_max = 3
    ffmpeg_stacked = ffmpeg_stacked_max
    prevPicName = ""
    curRecStat = False
    stopwatchActive = False
    while True:
        if not recording:
            ret, frame = cap.read()
            time.sleep(0.2)
            #cv2.imshow('binary', frame)
            #cv2.waitKey(1)
        else:
            time.sleep(1)
            lastPicName = getLastPicName()
            if not lastPicName:
                logger.warning("Can't determine lastPicName!")
                continue
            if prevPicName == lastPicName:
                ffmpeg_stacked -= 1
            else:
                ffmpeg_stacked = ffmpeg_stacked_max
            if ffmpeg_stacked == 0:
                logger.debug('ffmpeg unexpectedly stop')
                turn_on_led(LED_LIGHTS.YELLOW)
                cap = cv2.VideoCapture(-1)
                ffmpeg_stacked = ffmpeg_stacked_max
                recording = False
                continue
                #stop_ffmpeg(pipe)
            prevPicName = lastPicName

            frame = cv2.imread(lastPicName) 
        if frame == None :
            logger.warning("Can't find last picture: " + lastPicName) 
            continue           

        prevRecStat = curRecStat
        frameroi = cv2.cv.fromarray(frame[minH:maxH, minW:maxW].copy())
        curRecStat = checkIfStopwachOnFrame(frameroi, trackEdges)
        if curRecStat == None:
            curRecStat = prevRecStat
        if curRecStat == prevRecStat:
            stopwatchActive = curRecStat#False


        if (not recording) and stopwatchActive:
            recording = True
            cap.release()
            # удаляем девайс и объект с картинкой с камеры (походу
            # frame владеет памятью, которая должна быть освобождена)
            del cap
            del frame

            # time.sleep(1)
            logger.debug('run ffmpeg')
            # turn on green ligth
            turn_on_led(LED_LIGHTS.GREEN)
            # имя файла - records_dir + текущее время + расширение .mkv
            # TODO: в теории, records_dir вычисляется 1 раз и в процессе
            # записи место может кончится
            filename = records_dir + \
                       dt.datetime.now().strftime('%H-%M-%S') + '.mkv'
            pipe = run_ffmpeg(resolution, of=filename)

        elif recording and (not stopwatchActive):
            recording = False
            logger.debug('stop ffmpeg')
            #turn on yellow light
            turn_on_led(LED_LIGHTS.YELLOW)
            stop_ffmpeg(pipe)
            #time.sleep(1)
            cap = cv2.VideoCapture(-1)

        else:
            pass
            # logger.debug('Waiting... recording: %s, stopwatchActive: %s', \
            #                recording, stopwatchActive)


def copy_log_onto_flash(flash_path):
    """ Чисто дебажная штука, копирующая текущий лог в корень флэшки.
    TODO: Удалить.
    """
    shutil.copy(_LOG_FILENAME, flash_path)


def SIGTERM_handler(signum, frame):
    logger.critical('the process has got SIGTERM signal (shutdown?) with frame %s: ', frame)
    sys.exit(1)

if __name__ == '__main__':
    try:
        signal.signal(signal.SIGTERM, SIGTERM_handler)
        logger.info('================== Programm START  =====================')

        remove_tmp_photos()
        main()
        logger.info('Programm end\n\n\n')
    except SystemExit:
        logger.critical('Programm end\n\n\n')
    except:
        logger.exception("[!!] Uncought Exception in main!")
        logger.critical('Programm end\n\n\n')


