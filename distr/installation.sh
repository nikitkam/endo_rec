#!/bin/bash

#ffmpeg
#http://www.noobslab.com/2014/12/ffmpeg-returns-to-ubuntu-1410.html
sudo add-apt-repository ppa:kirillshkrogalev/ffmpeg-next
sudo apt-get -y update
sudo apt-get -y install ffmpeg

#dkms
sudo apt-get -y install dkms

#driver
sudo dpkg -i vga2usb-3.30.1.30-ubuntu-3.19.0-15-generic-x86_64.deb

#htop
sudo apt-get -y install htop

#opencv
sudo apt-get -y install python-opencv

# adding user into video group
sudo gpasswd -a $USER video

#install realpath
sudo apt-get install realpath

#copy project directory
src_dir=`realpath ..`
cp -r "$src_dir" "/home/$USER/"

# make .py files executable
dst_dir="/home/$USER/endo_rec"
chmod +x ${dst_dir}/*.py

#compile diodes binary
sudo gcc -O "${dst_dir}/turn_on_diode.c" -o "${dst_dir}/turn_on_diode"
sudo chmod 4755 "${dst_dir}/turn_on_diode"

#set python script to autoboot
sudo cp rc.local /etc/
sudo sed -i s/\$USER/$USER/g /etc/rc.local
echo "[+] rc.local is updated"

#set new parameter to pwer button

sudo cp powerbtn.sh /etc/acpi/powerbtn.sh
echo "[+] powerbtn.sh is installed"

