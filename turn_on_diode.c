#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/io.h>
#include <sys/types.h>
#include <fcntl.h>

#define BASEPORT 0x378 /* lp1 */

int main( int argc, char *argv[] ) {
	int a = setuid(0);
	if ( argc != 3 ){
		printf( "usage: %s port_code i/o\n", argv[0] );	
		exit(1);}

	char c, io;
	int n, tem, port, i = 0;
	unsigned char readReg = ' ';

	// check if input parameter is a digit
	if (sscanf (argv[1], "%i", &port)!=1) { printf ("error - 1st arg is not an integer"); exit(2); }
	if (sscanf (argv[2], "%c", &io)!=1) { printf ("error - 2nd arg is not a achar"); exit(3); }

	//set permissions to access port
	if (ioperm(BASEPORT, 3, 1)) {perror("ioperm"); exit(1);}
	
	tem = fcntl(0, F_GETFL, 0);
	fcntl (0, F_SETFL, (tem | O_NDELAY));
	
	fcntl(0, F_SETFL, tem);

	if (io == 'i'){
		printf("Turn on led with the code %d \n", port);
		outb(port, BASEPORT);
	}
	else if (io == 'o'){
		readReg = inb(BASEPORT);
		printf("reg=%d\n", readReg);
	}
	
	//take away permissions to access port
	if (ioperm(BASEPORT, 3, 0)) {perror("ioperm"); exit(1);}
	
	
}
