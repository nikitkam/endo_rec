#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime as dt
import glob
import logging
import os
import sha
import subprocess as sp


# ========================== настройка логирования =============================
import logging
from logging.handlers import RotatingFileHandler

logging.basicConfig(level=logging.DEBUG, fmt='')

# формат сообщений в логах
FORMATTER = logging.Formatter(
      fmt='%(asctime)s [%(name)s][%(levelname)s]: %(message)s'
    , datefmt='%d/%m/%Y %H:%M:%S'
)


_SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))

# хэндлер, который в лог пишет все
spam_h = RotatingFileHandler(
      filename=os.path.join(_SCRIPT_DIR, 'find_path.log')
    , maxBytes=50*1024*1024
    , backupCount=5
    , encoding='utf-8'
)
spam_h.setLevel(logging.DEBUG)
spam_h.setFormatter(FORMATTER)

logger = logging.getLogger(__name__)
logger.addHandler(spam_h)

# ======================== конец настройки логирования ========================



MIN_FREE_SPACE = 100*1024*1024


def get_mountpoints(candidates):
    '''
    Принимает на вход список папок, в которых обычно расположены
    точки монтирования.
    Возвращает список папок, которые являются точками монтирования.
    '''
    mountpoints = []
    candidates = [x for x in candidates if os.path.exists(x)]
    for dir_ in candidates:
        g = os.walk(dir_)
        dir_, subdirs, _ = g.next()
        subdirs = [os.path.join(dir_, x) for x in subdirs]
        for subdir in subdirs:
            if os.path.ismount(subdir):
                mountpoints.append(subdir)

    if len(mountpoints):
        logger.debug('Found mountpoints: %s', mountpoints)
    else:
        logger.warning('No mountpoints found!')

    return mountpoints


def is_enough_space(dir_):
    stat = os.statvfs(dir_)
    free_space = stat.f_bsize*stat.f_bfree
    logger.debug('free %d Mb on %s', free_space/1024/1024, dir_)
    if free_space > MIN_FREE_SPACE:
        return True
    return False


def is_write_allowed(dir_):
    tmp_name = os.path.abspath(dir_) + \
               os.path.sep + \
               sha.new('test').hexdigest()
    try:
        date_dir = dir_ + \
                   os.path.sep + \
                   dt.date.strftime(dt.date.today(), "%F")
        if not os.path.exists(date_dir):
            os.mkdir(date_dir)
        else:
            open(tmp_name, 'wb')
            os.remove(tmp_name)
        return True
    except:
        logger.exception("[-] Error accessing '%s'", dir_)
        return False



def get_records_dir():
    current_username = os.path.expandvars('$USER')
    MOUNT_DIRS = ['/mnt/', '/media/%s/' % current_username]
    mountpoints = get_mountpoints(MOUNT_DIRS)

    # отфитровываем папки, в которых есть свободное место
    enough_space_dirs = [dir_ for dir_ in mountpoints \
                                 if is_enough_space(dir_)]
    logger.debug("Dirs with enough space: %s", enough_space_dirs)

    # отфильтровываем папки, в которые разрешена запись
    writeable_dirs = [d for d in enough_space_dirs \
                                 if is_write_allowed(d)]
    logger.debug("Writeable dirs: %s", writeable_dirs)

    if not len(writeable_dirs):
        logger.warning("No suitable dirs found!")
        return None
    logger.debug('Any folder from this list is good enough: %s', writeable_dirs)

    for dir_ in writeable_dirs:
        records_dir = os.path.abspath(dir_) + \
                  os.path.sep + \
                  dt.date.strftime(dt.date.today(), "%F") + \
                  os.path.sep
        logger.info('Record dir is: %s', records_dir)
        return records_dir
    return None


def run_ffmpeg(of=None):
    FFMPEG_BIN = 'ffmpeg'

    if not of:
        of = 'video.mkv'

    command = [ FFMPEG_BIN,
                '-y',
                '-f', 'x11grab',
                '-r', '25',
                '-s', '1366x768',
                '-i', ':0.0',
                '-vcodec', 'libx264',
                of,
                '-r', '1',
                '-s', '1366x768',
                '-f', 'image2',
                'tmp%04d.png'
    ]

    # pipe = sp.Popen(command, stdout = sp.PIPE, stdin = sp.PIPE, bufsize=10**8)
    pipe = sp.Popen(command, stdin=sp.PIPE, bufsize=10**8)

    # pipe = sp.Popen(command, stdout = sp.PIPE, bufsize=10**8)

    print 'Popen\nWaiting...'

    return pipe

def stop_ffmpeg(pipe_):
    FFMPEG_STOP_KEY = 'q'
    pipe_.communicate(FFMPEG_STOP_KEY)

    def remove_temp_photos():
        for f in glob.glob('tmp*.png'):
            os.remove(f)

    remove_temp_photos()


if __name__ == '__main__':
    logger.info('\n\n\n================= START logging %s =================', __name__)
    records_dir = get_records_dir()

    # pipe = run_ffmpeg(records_dir + 'temp.mkv')
    # raw_input()
    # stop_ffmpeg(pipe)

